package com.ljph.seckill.common;

import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.TableMapping;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.ljph.seckill.model._MappingKit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by yuzhou on 16/8/1.
 */
 
public class BaseTest {

    private static final Logger _log = LoggerFactory.getLogger(BaseTest.class);

    protected static C3p0Plugin c3p0Plugin = null;
    protected static ActiveRecordPlugin arp = null;

    public BaseTest(){
        _log.info("BaseTest Constructor");
    }

    public static void start() {
        if(c3p0Plugin == null) {
            Prop p = PropKit.use("config/mysql.properties");
            _log.info(p.get("jdbcUrl"));
            c3p0Plugin = new C3p0Plugin(p.getProperties());
            c3p0Plugin.start();
        }

        if(arp == null) {
            arp = new ActiveRecordPlugin("testDs", c3p0Plugin);
            _MappingKit.mapping(arp);
            arp.setDevMode(true);
            arp.setShowSql(true);
            arp.start();
        }
    }

    public static void stop() {
        //arp.stop();
        //c3p0Plugin.stop();
    }
}
